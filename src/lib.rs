#![deny(missing_docs)]

/*!
A formatting type to allow combining various text formatting styles:

* `normal`
* `*bold*`
* `/italic/`
* `` `mono` ``
* `^superscript^`
* `|subscript|`
* `^|small|^`
* `_underscore_`
* `~strikethrough~`

All formattings can be combined using bitwise operations.
*/

use derive_more::derive::{BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign};

/// The default formatting used for rendering some text.
#[derive(Copy, Clone, Default, BitOr, BitAnd, BitXor, BitOrAssign, BitAndAssign, BitXorAssign)]
pub struct Formatting(u8);

impl Formatting {
    /// Represents bold formatting.
    pub const BOLD: Self = Self(0b00000001);

    /// Represents italic formatting.
    pub const ITALIC: Self = Self(0b00000010);

    /// Represents monospace formatting.
    pub const MONO: Self = Self(0b00000100);

    /// Represents superscript formatting.
    pub const TOP: Self = Self(0b00001000);

    /// Represents subscript formatting.
    pub const BOTTOM: Self = Self(0b00010000);

    /// Represents underscore formatting.
    pub const UNDERSCORE: Self = Self(0b00100000);

    /// Represents strikethrough formatting.
    pub const STRIKETHROUGH: Self = Self(0b01000000);

    /// Checks if the `Formatting` instance contains any of the specified formatting flags.
    ///
    /// # Examples
    ///
    /// Check if a `Formatting` instance contains specific formatting flags:
    ///
    /// ```
    /// use pukram_formatting::Formatting;
    ///
    /// let formatting = Formatting::BOLD | Formatting::ITALIC;
    /// assert!(formatting.contains(Formatting::BOLD));
    /// assert!(formatting.contains(Formatting::ITALIC));
    /// assert!(!formatting.contains(Formatting::MONO));
    /// ```
    #[inline]
    pub fn contains(self, formatting: Self) -> bool {
        (self & formatting).0 > 0
    }

    /// Checks if the `Formatting` instance has no formatting flags set.
    ///
    /// # Examples
    ///
    /// Check if a `Formatting` instance is empty:
    ///
    /// ```
    /// use pukram_formatting::Formatting;
    ///
    /// let formatting = Formatting::default();
    /// assert!(formatting.is_empty());
    ///
    /// let formatting = Formatting::BOLD | Formatting::ITALIC;
    /// assert!(!formatting.is_empty());
    /// ```
    #[inline]
    pub fn is_empty(self) -> bool {
        self.0 == 0
    }

    /// Checks if bold formatting is activated.
    #[inline]
    pub fn is_bold(self) -> bool {
        self.contains(Self::BOLD)
    }

    /// Checks if italic formatting is activated.
    #[inline]
    pub fn is_italic(self) -> bool {
        self.contains(Self::ITALIC)
    }

    /// Checks if monospace formatting is activated.
    #[inline]
    pub fn is_mono(self) -> bool {
        self.contains(Self::MONO)
    }

    /// Checks if superscript formatting is activated.
    #[inline]
    pub fn is_top(self) -> bool {
        self.contains(Self::TOP)
    }

    /// Checks if subscript formatting is activated.
    #[inline]
    pub fn is_bottom(self) -> bool {
        self.contains(Self::BOTTOM)
    }

    /// Checks if underscore formatting is activated.
    #[inline]
    pub fn is_underscore(self) -> bool {
        self.contains(Self::UNDERSCORE)
    }

    /// Checks if strikethrough formatting is activated.
    #[inline]
    pub fn is_strikethrough(self) -> bool {
        self.contains(Self::STRIKETHROUGH)
    }
}

impl Formatting {
    /// Apply character specific formatting and return if it has been changed.
    ///
    /// # Examples
    ///
    /// Apply formatting characters to a `Formatting` instance:
    ///
    /// ```
    /// use pukram_formatting::Formatting;
    ///
    /// let mut formatting = Formatting::default();
    /// assert!(formatting.is_empty());
    /// assert!(formatting.apply('*'));
    /// assert!(formatting.is_bold());
    /// assert!(formatting.apply('/'));
    /// assert!(formatting.is_italic());
    /// assert!(!formatting.apply('A'));
    /// assert!(formatting.apply('*'));
    /// assert!(!formatting.is_bold());
    /// ```
    pub fn apply(&mut self, c: char) -> bool {
        let mask = match c {
            '*' => Self::BOLD,
            '/' => Self::ITALIC,
            '`' => Self::MONO,
            '^' => Self::TOP,
            '|' => Self::BOTTOM,
            '_' => Self::UNDERSCORE,
            '~' => Self::STRIKETHROUGH,
            _ => return false,
        };

        *self ^= mask;

        true
    }
}
