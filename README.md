`pukram-formatting` provides a `Formatting` struct that allows combining various text formatting styles commonly used in the pukram markup language.

It supports the following formatting styles:

- `normal`
- `*bold*`
- `/italic/`
- `` `mono` ``
- `^superscript^`
- `|subscript|`
- `^|small|^`
- `_underscore_`
- `~strikethrough~`

All formattings can be combined using bitwise operations.
